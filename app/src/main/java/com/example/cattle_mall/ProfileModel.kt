package com.example.cattle_mall

import com.google.gson.annotations.SerializedName

data class ProfileModel (
    val name : String? = null,
    val phone : String? = null,
    val email : String? = null
)