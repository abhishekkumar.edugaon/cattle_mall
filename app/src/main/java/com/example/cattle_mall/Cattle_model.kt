package com.example.cattle_mall

data class Cattle_model(
    val sellername: String = "",
    val sellerid: String = "",
    var sellerdistrict: String = "",
    var sellerstate: String = "",
    var pregnancyStatus: String = "",
    var milkCapacity: String = "",
    var deliveryStatus: String = "",
    var currentMilk: String = "",
    var cattlePrice: String = "",
    var cattleDescription: String = "",
    var animalType: String = "",
    var animalLocatation: String = "",
    var animalGender: String = "",
    var sellid: String = "",
    var imageUrl : String? = null,
)
