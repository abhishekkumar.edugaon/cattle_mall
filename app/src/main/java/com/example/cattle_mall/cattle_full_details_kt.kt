package com.example.cattle_mall

import android.annotation.SuppressLint
import android.content.Intent
import android.icu.text.SimpleDateFormat
import android.media.Image
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Button
import android.widget.ImageView
import android.widget.TextView
import android.widget.Toast
import com.bumptech.glide.Glide
import com.google.android.gms.auth.api.Auth
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.firestore.FirebaseFirestore
import com.google.firebase.firestore.ktx.firestore
import com.google.firebase.firestore.ktx.toObject
import com.google.firebase.ktx.Firebase
import org.checkerframework.common.subtyping.qual.Bottom
import java.util.*

class cattle_full_details_kt : AppCompatActivity() {

    @SuppressLint("WrongViewCast", "MissingInflatedId", "SuspiciousIndentation")
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_cattle_full_details_kt)

        val backHome = findViewById<ImageView>(R.id.back_homepage)
        backHome.setOnClickListener {
        onBackPressed()
        }

        val logPref = getSharedPreferences("login", AppCompatActivity.MODE_PRIVATE)

        val itemId = intent.getStringExtra("id")
        val TitleNo1 = findViewById<TextView>(R.id.text_title)
        val CattlePrice = findViewById<TextView>(R.id.cattle_price)
        val SellersLocation = findViewById<TextView>(R.id.sellers_location)
        val CattleLactation = findViewById<TextView>(R.id.cattle_lactation)
        val CattleDelivery = findViewById<TextView>(R.id.cattle_delivery)
        val CattlePregnant = findViewById<TextView>(R.id.cattle_pregnant)
        val Cattle_Current_Milk = findViewById<TextView>(R.id.cattle_current_milk)
        val Cattle_Milk_Capacity = findViewById<TextView>(R.id.cattle_milk_capacity)
        val cattle_image = findViewById<ImageView>(R.id.cattle_image)
        val text_description = findViewById<TextView>(R.id.text_description)
        val btn=findViewById<Button>(R.id.buy_now)
        val id = logPref?.getString("userId", "")

                val firebase=Firebase.firestore
        firebase.collection("sell").document(itemId.toString()).get()
            .addOnSuccessListener {
                val data=it.toObject<Cattle_model>()
                TitleNo1.text=data?.animalType
                CattlePrice.text=data?.cattlePrice
                SellersLocation.text=data?.sellerdistrict+","+data?.sellerstate
                CattleLactation.text=data?.animalLocatation
                CattleDelivery.text=data?.deliveryStatus
                CattlePregnant.text=data?.pregnancyStatus
                Cattle_Current_Milk.text=data?.currentMilk
                Cattle_Milk_Capacity.text=data?.milkCapacity
                text_description.text=data?.cattleDescription

                Glide.with(this).load(data?.imageUrl).into(cattle_image)

                val random = Random()
                fun rand(from: Int, to: Int) : Int {
                    return random.nextInt(to - from) + from
                }

                btn.setOnClickListener {
                    val sdf = SimpleDateFormat("dd-MM-yyyy").format(Date())

                    val order = hashMapOf(
                        "order_id" to random.toString(),
                        "client_id" to id,
                        "sell_id" to data?.sellid.toString(),
                        "order_date" to sdf,
                        "seller_id" to data?.sellerid.toString(),
                        "cattleImage" to data?.imageUrl,
                        "animalType" to data?.animalType,
                        "cattleDescription" to data?.cattleDescription,
                    )
                    FirebaseFirestore.getInstance().collection("order").document(random.toString()).set(order)
                        .addOnSuccessListener {
                            FirebaseFirestore.getInstance().collection("sell").document(data?.sellid.toString()).update("type","buy")

                            Toast.makeText(this,"order successful", Toast.LENGTH_SHORT).show()

                                val intent= Intent(this,BottomNavigationBarKt::class.java)
                                startActivity(intent)
                                finish()
                        }


                }
            }
            .addOnFailureListener {
                Toast.makeText(this, "failed", Toast.LENGTH_SHORT).show()
            }




}
}