package com.example.cattle_mall

data class ViewYourCattleModel(
    var imageUrl : String? = null,
    var cattleDescription : String? = null,
    var animalType : String? = null,
    var sellid : String? = null,

)
