package com.example.cattle_mall

import android.annotation.SuppressLint
import android.content.Intent
import android.net.Uri
import android.os.Bundle
import android.provider.MediaStore
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.*
import androidx.activity.result.ActivityResultCallback
import androidx.activity.result.contract.ActivityResultContracts
import androidx.appcompat.app.AppCompatActivity
import androidx.cardview.widget.CardView
import androidx.core.app.ActivityCompat.startActivityForResult
import com.google.android.material.bottomsheet.BottomSheetDialog
import com.google.android.material.textfield.TextInputEditText
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.database.FirebaseDatabase
import com.google.firebase.firestore.FirebaseFirestore
import com.google.firebase.ktx.Firebase
import com.google.firebase.storage.FirebaseStorage
import com.google.firebase.storage.ktx.storage
import java.util.*

class NotificationFragment : Fragment() {
    lateinit var firebase_sell_auth: FirebaseAuth

    private var storageRef = Firebase.storage

    private lateinit var showImage: ImageView
    private lateinit var browseImage: Button
    private lateinit var uploadImage: Button

    private lateinit var uri : Uri

    var selectedAnimal = ""
    var selectedLocatation = ""
    var selectedDelivery = ""
    var selectedPregnancy = ""
    var selectedGender = ""
    var imageUrl = ""

    @SuppressLint("CutPasteId", "MissingInflatedId")
    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {

        // Inflate the layout for this fragment
        var rooView = inflater.inflate(R.layout.fragment_notification, container, false)

        //Animal type Spinner
        val arrayAdapter: ArrayAdapter<*>
        val cattleType = arrayOf(
            "Select Animal",
            "Cow",
            "Buffalo",
            "Ox",
            "Goat",
            "Sheep",
            "Ass",
            "Horse",
            "Dog",
            "Rabbit",
            "Camel",
            "Elephant",
            "Others"
        )

        var cattleSpinner = rooView.findViewById<Spinner>(R.id.animal_type_spinner)

        val context = activity
        if (context != null) {
            arrayAdapter = ArrayAdapter(
                context,
                com.google.android.material.R.layout.support_simple_spinner_dropdown_item,
                cattleType
            )
            cattleSpinner.adapter = arrayAdapter
        }


        cattleSpinner.onItemSelectedListener = object :
            AdapterView.OnItemSelectedListener {
            override fun onItemSelected(p0: AdapterView<*>?, p1: View?, p2: Int, p3: Long) {
//                Toast.makeText(activity,"${cattleType[p2]}", Toast.LENGTH_SHORT).show()
                selectedAnimal = cattleType[p2]
            }

            override fun onNothingSelected(p0: AdapterView<*>?) {
            }

        }

        //lactation Spinner
        val arrayAdapter2: ArrayAdapter<*>
        val cattleLocatation = arrayOf(
            "Select Pregnancy",
            "Not yet (0)",
            "First (1)",
            "Second (2)",
            "Third (3)",
            "Four (4)",
            "Five (5)",
            "Six (6)",
            "Seven (7)"
        )

        var lactationSpinner = rooView.findViewById<Spinner>(R.id.animal_lactation_spinner)

        val context2 = activity
        if (context2 != null) {
            arrayAdapter2 = ArrayAdapter(
                context2,
                com.google.android.material.R.layout.support_simple_spinner_dropdown_item,
                cattleLocatation
            )
            lactationSpinner.adapter = arrayAdapter2
        }


        lactationSpinner.onItemSelectedListener = object :
            AdapterView.OnItemSelectedListener {
            override fun onItemSelected(p0: AdapterView<*>?, p1: View?, p2: Int, p3: Long) {
//                Toast.makeText(activity,"${cattleLocatation[p2]}", Toast.LENGTH_SHORT).show()
                selectedLocatation = cattleLocatation[p2]
            }

            override fun onNothingSelected(p0: AdapterView<*>?) {
            }

        }

        //delivered baby Spinner
        val arrayAdapter3: ArrayAdapter<*>
        val deliveryStatus = arrayOf("Select delivery status", "Yes", "No")

        var deliverySpinner = rooView.findViewById<Spinner>(R.id.animal_delivered_bay_spinner)

        val context3 = activity
        if (context3 != null) {
            arrayAdapter3 = ArrayAdapter(
                context3,
                com.google.android.material.R.layout.support_simple_spinner_dropdown_item,
                deliveryStatus
            )
            deliverySpinner.adapter = arrayAdapter3
        }


        deliverySpinner.onItemSelectedListener = object :
            AdapterView.OnItemSelectedListener {
            override fun onItemSelected(p0: AdapterView<*>?, p1: View?, p2: Int, p3: Long) {
//                Toast.makeText(activity,"${deliveryStatus[p2]}", Toast.LENGTH_SHORT).show()
                selectedDelivery = deliveryStatus[p2]

            }

            override fun onNothingSelected(p0: AdapterView<*>?) {
            }

        }

        //pregnancy Spinner
        val arrayAdapter4: ArrayAdapter<*>
        val pregnancyStatus = arrayOf("Choose pregnancy status", "Yes", "No")

        var pregnancySpinner = rooView.findViewById<Spinner>(R.id.animal_pregnancy_status_spinner)

        val context4 = activity
        if (context4 != null) {
            arrayAdapter4 = ArrayAdapter(
                context4,
                com.google.android.material.R.layout.support_simple_spinner_dropdown_item,
                pregnancyStatus
            )
            pregnancySpinner.adapter = arrayAdapter4
        }


        pregnancySpinner.onItemSelectedListener = object :
            AdapterView.OnItemSelectedListener {
            override fun onItemSelected(p0: AdapterView<*>?, p1: View?, p2: Int, p3: Long) {
//                Toast.makeText(activity,"${pregnancyStatus[p2]}", Toast.LENGTH_SHORT).show()
                selectedPregnancy = pregnancyStatus[p2]
            }

            override fun onNothingSelected(p0: AdapterView<*>?) {
            }

        }

        //Animal calf Spinner
        val arrayAdapter5: ArrayAdapter<*>
        val cattlecalfGender = arrayOf("Select", "Female Calf", "Male Calf", "No Calf")

        var calfSpinner = rooView.findViewById<Spinner>(R.id.animal_calf_spinner)

        val context5 = activity
        if (context5 != null) {
            arrayAdapter5 = ArrayAdapter(
                context5,
                com.google.android.material.R.layout.support_simple_spinner_dropdown_item,
                cattlecalfGender
            )
            calfSpinner.adapter = arrayAdapter5
        }


        calfSpinner.onItemSelectedListener = object :
            AdapterView.OnItemSelectedListener {
            override fun onItemSelected(p0: AdapterView<*>?, p1: View?, p2: Int, p3: Long) {
//                Toast.makeText(activity,"${cattlecalfGender[p2]}", Toast.LENGTH_SHORT).show()
                selectedGender = cattlecalfGender[p2]
            }

            override fun onNothingSelected(p0: AdapterView<*>?) {
            }

        }

        // sell store data
        firebase_sell_auth = FirebaseAuth.getInstance()

        var firebaseFirestore = FirebaseFirestore.getInstance()
        val sellPref = activity?.getSharedPreferences("login", AppCompatActivity.MODE_PRIVATE)
        val userData = activity?.getSharedPreferences("data", AppCompatActivity.MODE_PRIVATE)


        val sellerId = sellPref?.getString("userId", "")
        val sellernname = userData?.getString("Name", "")
        val sellersstate = userData?.getString("State", "")
        val sellerddistrict = userData?.getString("District", "")


        val currentMilk = rooView.findViewById<EditText>(R.id.current_milk_edit_text)
        val milkCapacity = rooView.findViewById<EditText>(R.id.milk_capacity_edit_text)
        val cattlePrice = rooView.findViewById<EditText>(R.id.animal_price_edit_text)
        val cattleDiscription = rooView.findViewById<EditText>(R.id.description)

        val finishButton = rooView.findViewById<Button>(R.id.finish_button)

        finishButton.setOnClickListener {
            if (currentMilk.text.isEmpty() == false && milkCapacity.text.isEmpty() == false && cattlePrice.text.isEmpty() == false && cattleDiscription.text.isEmpty() == false
                && selectedAnimal.isEmpty() == false && selectedLocatation.isEmpty() == false && selectedDelivery.isEmpty() == false && selectedPregnancy.isEmpty() == false && selectedGender.isEmpty() == false
            ) {
                val random = Random()
                fun rand(from: Int, to: Int) : Int {
                    return random.nextInt(to - from) + from
                }


                val currentmilk = currentMilk.text.toString().trim()
                val milkcapacity = milkCapacity.text.toString().trim()
                val cattleprice = cattlePrice.text.toString().trim()
                val cattlediscription = cattleDiscription.text.toString().trim()
                val animaltype = selectedAnimal.trim()
                val animallocatation = selectedLocatation.trim()
                val animaldelivery = selectedDelivery.trim()
                val animalpregnancy = selectedPregnancy.trim()
                val animalgender = selectedGender.trim()
                val sellerName = sellernname.toString()
                val sellerState = sellersstate.toString()
                val sellerDistrict = sellerddistrict.toString()
                val sellerId = sellerId.toString()
                val id = random.toString()



                val sellMap = hashMapOf(
                    "animalType" to animaltype,
                    "animalLocatation" to animallocatation,
                    "currentMilk" to currentmilk,
                    "milkCapacity" to milkcapacity,
                    "cattlePrice" to cattleprice,
                    "deliveryStatus" to animaldelivery,
                    "pregnancyStatus" to animalpregnancy,
                    "animalGender" to animalgender,
                    "cattleDescription" to cattlediscription,
                    "imageUrl" to imageUrl,
                    "sellername"   to sellerName,
                    "sellerstate"   to sellerState,
                    "sellerdistrict"   to sellerDistrict,
                    "sellerid"   to sellerId,
                    "sellid" to id,
                    "type" to "sell"
                )

                FirebaseFirestore.getInstance().collection("sell").document(random.toString()).set(sellMap)
                    .addOnSuccessListener {
                        Toast.makeText(activity, "sell successfully", Toast.LENGTH_SHORT).show()
                        activity.let {
                            val intent = Intent(it, BottomNavigationBarKt::class.java)
                            startActivity(intent)
                        }

                    }
                    .addOnFailureListener {
                        Toast.makeText(activity, "please fill all data", Toast.LENGTH_SHORT).show()
                    }

            } else {
                Toast.makeText(
                    activity,
                    "something went wrong please fill all data",
                    Toast.LENGTH_SHORT
                ).show()
            }
        }


        storageRef = FirebaseStorage.getInstance()

        showImage = rooView.findViewById(R.id.show_mage)
        browseImage = rooView.findViewById(R.id.browse_mage)
        uploadImage = rooView.findViewById(R.id.upload_image)

        val galleryImage = registerForActivityResult(
            ActivityResultContracts.GetContent(),
            ActivityResultCallback {
                showImage.setImageURI(it)
                uri = it!!
            }
        )

        browseImage.setOnClickListener {
            galleryImage.launch("image/*")
        }

        uploadImage.setOnClickListener {
            storageRef.getReference("images").child(System.currentTimeMillis().toString())
                .putFile(uri)
                .addOnSuccessListener { task ->
                    task.metadata!!.reference!!.downloadUrl
                        .addOnSuccessListener {
                            val userId = FirebaseAuth.getInstance().currentUser?.uid
                            imageUrl = it.toString()
                            val mapImage = mapOf(
                                "url" to it.toString()
                            )

                            val databaseReferance = FirebaseDatabase.getInstance().getReference("userImages")
                            userId?.let { it1 ->
                                databaseReferance.child(it1).setValue(mapImage)
                                    .addOnSuccessListener {
                                        Toast.makeText(activity, "Successful", Toast.LENGTH_SHORT).show()
                                    }
                                    .addOnFailureListener {error ->
                                        Toast.makeText(activity, it.toString(), Toast.LENGTH_SHORT).show()
                                    }
                            }
                        }
                }
        }


        // your cattle view code start

        val yourCattle = rooView.findViewById<CardView>(R.id.your_cattle)

        yourCattle.setOnClickListener {
            val intent = Intent(activity, ViewYourCattleSellsKt::class.java)
            startActivity(intent)
        }


        // your cattle view code end
        return rooView
    }
    }
