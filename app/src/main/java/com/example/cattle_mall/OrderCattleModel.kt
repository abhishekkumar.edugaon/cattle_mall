package com.example.cattle_mall

data class OrderCattleModel(
    var cattleImage : String? = null,
    var animalType : String? = null,
    var cattleDescription : String? = null,
    var order_id : String? = null,
    var sell_id : String? = null,
    var seller_id : String? = null,
    var client_id : String? = null,

)
