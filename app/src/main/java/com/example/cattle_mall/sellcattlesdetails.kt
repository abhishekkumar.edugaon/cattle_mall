package com.example.cattle_mall

import android.annotation.SuppressLint
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.ImageView
import android.widget.TextView
import com.bumptech.glide.Glide
import com.google.firebase.firestore.ktx.firestore
import com.google.firebase.firestore.ktx.toObject
import com.google.firebase.ktx.Firebase

class sellcattlesdetails : AppCompatActivity() {
    @SuppressLint("WrongViewCast")
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_sellcattlesdetails)

        val backHome = findViewById<ImageView>(R.id.back_your_cattle)
        backHome.setOnClickListener {
            onBackPressed()
        }


        val itemId = intent.getStringExtra("id")
        val TitleNo1 = findViewById<TextView>(R.id.your_cattle_text_title)
        val CattlePrice = findViewById<TextView>(R.id.your_cattle_price)
        val SellersLocation = findViewById<TextView>(R.id.your_cattle_location)
        val CattleLactation = findViewById<TextView>(R.id.your_cattle_lactation)
        val CattleDelivery = findViewById<TextView>(R.id.your_cattle_delivery)
        val CattlePregnant = findViewById<TextView>(R.id.your_cattle_pregnant)
        val Cattle_Current_Milk = findViewById<TextView>(R.id.your_cattle_current_milk)
        val Cattle_Milk_Capacity = findViewById<TextView>(R.id.your_cattle_milk_capacity)
        val cattle_image = findViewById<ImageView>(R.id.your_cattle_image_layout_)
        val text_description = findViewById<TextView>(R.id.your_cattle_description)

        val firebase = Firebase.firestore
        firebase.collection("sell").document(itemId.toString()).get()
            .addOnSuccessListener {
                val data = it.toObject<Cattle_model>()

                TitleNo1.text = data?.animalType
                CattlePrice.text = data?.cattlePrice
                SellersLocation.text = data?.sellerdistrict + "," + data?.sellerstate
                CattleLactation.text = data?.animalLocatation
                CattleDelivery.text = data?.deliveryStatus
                CattlePregnant.text = data?.pregnancyStatus
                Cattle_Current_Milk.text = data?.currentMilk
                Cattle_Milk_Capacity.text = data?.milkCapacity
                text_description.text = data?.cattleDescription

                Glide.with(this).load(data?.imageUrl).into(cattle_image)


            }

    }
}