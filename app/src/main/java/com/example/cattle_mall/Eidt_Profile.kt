package com.example.cattle_mall

import android.annotation.SuppressLint
import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Button
import android.widget.EditText
import android.widget.Toast
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.firestore.FirebaseFirestore
import com.google.firebase.firestore.ktx.firestore
import com.google.firebase.ktx.Firebase
class Eidt_Profile : AppCompatActivity() {
    val firestore = FirebaseFirestore.getInstance()
    @SuppressLint("WrongViewCast", "MissingInflatedId", "SetTextI18n")
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_eidt_profile)


        val updateName = findViewById<EditText>(R.id.Update_Edit)
        val updateVillage = findViewById<EditText>(R.id.Update_Edit1)
        val updateState = findViewById<EditText>(R.id.Update_Edit4)
        val updatePin = findViewById<EditText>(R.id.Update_Edit5)
        val updateDistrict = findViewById<EditText>(R.id.Update_Edit6)



        val logPref = getSharedPreferences("login", AppCompatActivity.MODE_PRIVATE)
        val id = logPref?.getString("userId", "")
        val auth = FirebaseAuth.getInstance().currentUser?.uid
        if (auth != null) {
            FirebaseFirestore.getInstance().collection("users").document(auth).get()
                .addOnSuccessListener {
                    val document = it.data
                    updateName.setText("" + document?.get("username"))
                    updateVillage.setText("" + document?.get("village"))
                    updateState.setText("" + document?.get("state"))
                    updatePin.setText("" + document?.get("pin"))
                    updateDistrict.setText("" + document?.get("district"))
                }

//            updateBtn.setOnClickListener {
//
//                val usermap = hashMapOf(
//                    "username" to updateName,
//                    "email" to updateEmail,
//                    "state" to updateState
//                )
//                val auth = FirebaseAuth.getInstance().uid
//                if (auth != null) {
//                    FirebaseFirestore.getInstance().collection("users").document(auth).update(
//                        usermap as Map<String, Any>
//                    )
//                        .addOnSuccessListener {
//                            startActivity(Intent(this, ProfileFragment::class.java))
////                            val document=it.data
////                            updateName.setText("" + document?.get("username"))
////                            updateEmail.setText("" + document?.get("email"))
////                            updateState.setText("" + document?.get("state"))
//                        }
//                }
//            }

                    val updatabty = findViewById<Button>(R.id.butten_bty)
                    updatabty.setOnClickListener {
                        val Name = updateName.text.toString().trim()
                        val Village = updateVillage.text.toString().trim()
                        val State = updateState.text.toString().trim()
                        val PinCord = updatePin.text.toString().trim()
                        val District = updateDistrict.text.toString().trim()

                        val usermap = hashMapOf(
                            "username" to Name,
                            "village" to Village,
                            "state" to State,
                            "pin" to PinCord,
                            "district" to District,
                        )
                        firestore.collection("users").document(id.toString())
                            .update(usermap as Map<String, Any>)
                            .addOnSuccessListener {
                                Toast.makeText(this, "Update successful", Toast.LENGTH_SHORT).show()
                                val intent = Intent(this,BottomNavigationBarKt::class.java)
                                startActivity(intent)
                                finish()

                            }
                            .addOnFailureListener {

                            }
                    }
                }
            }
        }
