package com.example.cattle_mall

import android.annotation.SuppressLint
import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Button
import android.widget.EditText
import android.widget.Toast
import androidx.cardview.widget.CardView
import com.google.android.material.textfield.TextInputEditText
import com.google.android.material.textfield.TextInputLayout
import com.google.api.Context
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.firestore.FirebaseFirestore
import com.google.firebase.ktx.Firebase
import com.google.rpc.context.AttributeContext.Auth

class register_page_kt : AppCompatActivity() {

    lateinit var auth : FirebaseAuth

    @SuppressLint("MissingInflatedId", "CutPasteId", "SuspiciousIndentation")
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_register_page_kt)

        auth = FirebaseAuth.getInstance()

        var firebaseFirestore= FirebaseFirestore.getInstance()
        val logPref = getSharedPreferences("login", MODE_PRIVATE)
        val userData = getSharedPreferences("data", MODE_PRIVATE)


        val Name=findViewById<TextInputEditText>(R.id.user_name)
        val Phone=findViewById<TextInputEditText>(R.id.user_phone_number)
        val Email=findViewById<TextInputEditText>(R.id.user_email)
        val Password=findViewById<TextInputEditText>(R.id.password)
        val State=findViewById<TextInputEditText>(R.id.user_state)
        val District=findViewById<TextInputEditText>(R.id.user_District)
        val Village=findViewById<TextInputEditText>(R.id.user_village)
        val PinCode=findViewById<TextInputEditText>(R.id.user_pincode)

        val button=findViewById<Button>(R.id.submit_button)



                button.setOnClickListener {

                    if (Name.text?.isEmpty() == false && Phone.text?.isEmpty() == false && Email.text?.isEmpty() == false &&
                        Password.text?.isEmpty() == false && State.text?.isEmpty() == false && District.text?.isEmpty() == false &&
                        Village.text?.isEmpty() == false && PinCode.text?.isEmpty() == false
                    ) {


                        val name = Name.text.toString().trim()
                        val phone = Phone.text.toString().trim()
                        val email = Email.text.toString().trim()
                        val password = Password.text.toString().trim()
                        val state = State.text.toString().trim()
                        val district = District.text.toString().trim()
                        val village = Village.text.toString().trim()
                        val pinCode = PinCode.text.toString().trim()


                        val usermap = hashMapOf(
                            "username" to name,
                            "number" to phone,
                            "email" to email,
                            "password" to password,
                            "state" to state,
                            "district" to district,
                            "village" to village,
                            "pin" to pinCode,
                        )
                        auth.createUserWithEmailAndPassword(
                            Email.text.toString(),
                            Password.text.toString()

                        )
                            .addOnSuccessListener {

                                logPref.edit().putString("userId",it.user?.uid ).commit()


                            val id =logPref.getString("userId","")

                                firebaseFirestore.collection("users").document("${ it.user?.uid }").set(usermap)

                                    .addOnSuccessListener {
                                        Toast.makeText(this, " Signup Successfully", Toast.LENGTH_SHORT).show()

                                        userData.edit().putString("Name",name).commit()
                                        userData.edit().putString("State",state).commit()
                                        userData.edit().putString("District",district).commit()



                                        Name.text?.clear()
                                        Phone.text?.clear()
                                        Email.text?.clear()
                                        Password.text?.clear()
                                        State.text?.clear()
                                        District.text?.clear()
                                        Village.text?.clear()
                                        PinCode.text?.clear()

                                        //create shredpreferences for  user keep login
                                        logPref.edit().putBoolean("lStatuskey", true).commit()


                                        // after signup user go to bottomnavigationbar
                                        val intent = Intent(this, BottomNavigationBarKt::class.java)
                                        startActivity(intent)
                                        finish()


                                    }
                                    .addOnFailureListener {
                                        Toast.makeText(this, "failed", Toast.LENGTH_SHORT).show()


                                    }
                            }
                            }
                        else {
                            Toast.makeText(this, "Please fill all data", Toast.LENGTH_SHORT).show()
                        }

                    }



//        val submitButton = findViewById<Button>(R.id.submit_button)
//        submitButton.setOnClickListener {
//            val intent = Intent(this,BottomNavigationBarKt::class.java)
//            startActivity(intent)
//            finish()
//        }
    }
}