package com.example.cattle_mall

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide

class CattleOrderAdapter(val context: ProfileFragment, private val orderCattleModel: List<OrderCattleModel>, private var onOrderItemClick: ProfileFragment):
    RecyclerView.Adapter<CattleOrderAdapter.ViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): CattleOrderAdapter.ViewHolder {
        val itemView = LayoutInflater.from(parent.context).inflate(R.layout.order_list_item,parent, false)
        return CattleOrderAdapter.ViewHolder(itemView)
    }

    override fun getItemCount(): Int {
        return orderCattleModel.size
    }

    override fun onBindViewHolder(holder: CattleOrderAdapter.ViewHolder, position: Int) {
        val currentItem = orderCattleModel[position]

        holder.ordersCattleName.text = currentItem.animalType
        holder.ordersCattleDescription.text = currentItem.cattleDescription
        Glide.with(context).load(currentItem.cattleImage).into(holder.ordersCattleImage)


//        get image from firebase store using glide


//        holder.description.setOnClickListener {
//            homeItemClickListener.onHomeItemClick(currentItem)
//        }
        holder.itemView.setOnClickListener {
            onOrderItemClick.onOrderItemClick(currentItem)

        }
    }


    class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView){

        val ordersCattleImage : ImageView = itemView.findViewById(R.id.card_cattle_image)
        val ordersCattleName : TextView = itemView.findViewById(R.id.order_cattle_name)
        val ordersCattleDescription : TextView = itemView.findViewById(R.id.order_cattle_description)

    }
}

interface OnOrderItemClick {
    fun  onOrderItemClick(currentItem: OrderCattleModel)
}
