package com.example.cattle_mall

import android.annotation.SuppressLint
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import android.widget.Toast
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide


class HomeCattleAdapter(val context: HomeFragment, private val homeCattleModel: List<HomeCattleModel>, private var homeItemClickListener: HomeItemClickListener): RecyclerView.Adapter<HomeCattleAdapter.ViewHolder>() {


    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val itemView = LayoutInflater.from(parent.context).inflate(R.layout.recycler_view_layout,parent, false)
        return ViewHolder(itemView)
    }

    override fun getItemCount(): Int {
        return homeCattleModel.size
    }

    @SuppressLint("SuspiciousIndentation", "SetTextI18n")
    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
       val currentItem = homeCattleModel[position]

        holder.title.text = currentItem.animalType
        holder.price.text = currentItem.cattlePrice
        holder.description.text = currentItem.cattleDescription
        holder.sellername.text = currentItem.sellername
        holder.selleraddress.text = currentItem.sellerstate+", "+currentItem.sellerdistrict


        //get image from firebase store using glide
        Glide.with(context).load(currentItem.imageUrl).into(holder.imageView)


//        holder.description.setOnClickListener {
//            homeItemClickListener.onHomeItemClick(currentItem)
//        }
            holder.itemView.setOnClickListener {
            homeItemClickListener.onHomeItemClick(currentItem)

            }

    }

    class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView){

        val title : TextView = itemView.findViewById(R.id.cattle_name)
        val price : TextView = itemView.findViewById(R.id.cattle_price)
        val description : TextView = itemView.findViewById(R.id.text_descriptionn)
        val imageView : ImageView = itemView.findViewById(R.id.cattle_show_image)
        val sellername : TextView = itemView.findViewById(R.id.sellername)
        val selleraddress : TextView = itemView.findViewById(R.id.address)



    }
}

interface HomeItemClickListener {
    fun  onHomeItemClick(currentItem: HomeCattleModel)
}