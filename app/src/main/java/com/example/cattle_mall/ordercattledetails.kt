package com.example.cattle_mall

import android.annotation.SuppressLint
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.ImageView
import android.widget.TextView
import com.bumptech.glide.Glide
import com.google.firebase.firestore.ktx.firestore
import com.google.firebase.firestore.ktx.toObject
import com.google.firebase.ktx.Firebase
import java.util.Objects

class ordercattledetails : AppCompatActivity() {
    @SuppressLint("MissingInflatedId")
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_ordercattledetails)


        val itemId = intent.getStringExtra("sell_id")
        val TitleNo1 = findViewById<TextView>(R.id.order_page_text_title)
        val CattlePrice = findViewById<TextView>(R.id.order_page_cattle_price)
        val SellersLocation = findViewById<TextView>(R.id.order_page_sellers_location)
        val CattleLactation = findViewById<TextView>(R.id.order_page_cattle_lactation)
        val CattleDelivery = findViewById<TextView>(R.id.order_page_cattle_delivery)
        val CattlePregnant = findViewById<TextView>(R.id.order_page_cattle_pregnant)
        val Cattle_Current_Milk = findViewById<TextView>(R.id.order_page_cattle_current_milk)
        val Cattle_Milk_Capacity = findViewById<TextView>(R.id.order_page_cattle_milk_capacity)
        val cattle_image = findViewById<ImageView>(R.id.order_page_cattle_image)
        val text_description = findViewById<TextView>(R.id.order_page_text_description)

        val firebase = Firebase.firestore
        firebase.collection("sell").document(itemId.toString()).get()
            .addOnSuccessListener {
                val data = it.toObject<Cattle_model>()

                TitleNo1.text = data?.animalType
                CattlePrice.text = data?.cattlePrice
                SellersLocation.text = data?.sellerdistrict + "," + data?.sellerstate
                CattleLactation.text = data?.animalLocatation
                CattleDelivery.text = data?.deliveryStatus
                CattlePregnant.text = data?.pregnancyStatus
                Cattle_Current_Milk.text = data?.currentMilk
                Cattle_Milk_Capacity.text = data?.milkCapacity
                text_description.text = data?.cattleDescription

                Glide.with(this).load(data?.imageUrl).into(cattle_image)


            }
        val back = findViewById<ImageView>(R.id.order_back_page)
        back.setOnClickListener {
            onBackPressed()
        }

    }
}
