package com.example.cattle_mall

import android.annotation.SuppressLint
import android.content.Intent
import android.content.SharedPreferences
import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.RelativeLayout
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.firestore.Filter
import com.google.firebase.firestore.FirebaseFirestore

class HomeFragment : Fragment(), HomeItemClickListener {
    private lateinit var firebaseFirestore: FirebaseFirestore
    private lateinit var adapter: HomeCattleAdapter
    private lateinit var recyclerView: RecyclerView
    private lateinit var itemArrayList: ArrayList<HomeCattleModel>
    private lateinit var logPref: SharedPreferences


    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?,
    ): View? {
        logPref = activity?.getSharedPreferences("login", AppCompatActivity.MODE_PRIVATE)!!

        //firebase firestore initialization
        firebaseFirestore = FirebaseFirestore.getInstance()
        // Inflate the layout for this fragment
        val rootView = inflater.inflate(R.layout.fragment_home, container, false)

        val cow = rootView.findViewById<RelativeLayout>(R.id.cattle_cow)
        cow.setOnClickListener {
            loadAnimalByType("Cow")

        }
        val buffalo = rootView.findViewById<RelativeLayout>(R.id.cattle_buffalo)
        buffalo.setOnClickListener {
            loadAnimalByType("Buffalo")

        }

        return rootView
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        val layoutManager = LinearLayoutManager(context)
        recyclerView = view.findViewById(R.id.recyclerView)
        recyclerView.layoutManager = layoutManager
        dataInitialize()
    }

    @SuppressLint("ResourceType", "NotifyDataSetChanged")
    private fun dataInitialize() {
        val id = logPref.getString("userId", "")

        itemArrayList = ArrayList<HomeCattleModel>()
        firebaseFirestore.collection("sell")
            .whereNotEqualTo("sellerid", FirebaseAuth.getInstance().currentUser?.uid)
//            .whereEqualTo("type", "sell")
            .get()
            .addOnSuccessListener {
                itemArrayList.clear()
                val result = it.toObjects(HomeCattleModel::class.java)
                result.forEach {
                    if (it.type == "sell") {
                        itemArrayList.add(it)
                    }
                }

                adapter = activity?.let { HomeCattleAdapter(this, itemArrayList, this) }!!
                recyclerView.adapter = adapter
            }
            .addOnFailureListener {
                Toast.makeText(activity, it.message, Toast.LENGTH_SHORT).show()
            }

    }

    @SuppressLint("ResourceType", "NotifyDataSetChanged")
    private fun loadAnimalByType(animalType: String) {
        itemArrayList = arrayListOf<HomeCattleModel>()
        firebaseFirestore.collection("sell")
            .whereNotEqualTo("sellerid", FirebaseAuth.getInstance().currentUser?.uid)
//            .whereEqualTo("type", "sell")
            .get()
            .addOnSuccessListener {
                itemArrayList.clear()
                val result = it.toObjects(HomeCattleModel::class.java)
                result.forEach { homeDetaild ->
                    if (homeDetaild.type == "sell" && homeDetaild.animalType== animalType) {
                        itemArrayList.add(homeDetaild)
                    }
                }
                adapter = activity?.let { it1 -> HomeCattleAdapter(this, itemArrayList, this) }!!
                recyclerView.adapter = adapter
            }
            .addOnFailureListener {
                Toast.makeText(activity, it.message, Toast.LENGTH_SHORT).show()
            }


    }

    override fun onHomeItemClick(currentItem: HomeCattleModel) {
        activity.let {
            val intent = Intent(it, cattle_full_details_kt::class.java)
            intent.putExtra("id", currentItem.sellid)
            startActivity(intent)
        }
    }
}