package com.example.cattle_mall

import android.content.Context
import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.os.Handler
import android.os.Looper
import android.view.WindowManager

class splash_screen : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.splash_screen)


        val logPref = getSharedPreferences("login", Context.MODE_PRIVATE)
        val loginStatus= logPref.getBoolean("lStatuskey",false )

        Handler().postDelayed( {
            if ( loginStatus == true){
                val intent = Intent(this, BottomNavigationBarKt::class.java)
                startActivity(intent)
                finish()
            }

            else {
                val intent = Intent(this, login_page::class.java)
                startActivity(intent)
                finish()
            }

        },1000)


    }
}