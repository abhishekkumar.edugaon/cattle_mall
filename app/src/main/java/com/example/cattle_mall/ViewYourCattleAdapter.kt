package com.example.cattle_mall

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide


class ViewYourCattleAdapter(val context: Context, private val viewYourCattleModel: ArrayList<ViewYourCattleModel>, private var onYourCattleItemClick: OnYourCattleItemClick): RecyclerView.Adapter<ViewYourCattleAdapter.ViewHolder>() {
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewYourCattleAdapter.ViewHolder {
        val itemView = LayoutInflater.from(parent.context).inflate(R.layout.order_list_item,parent, false)
        return ViewYourCattleAdapter.ViewHolder(itemView)
    }

    override fun getItemCount(): Int {
        return viewYourCattleModel.size
    }

    override fun onBindViewHolder(holder: ViewYourCattleAdapter.ViewHolder, position: Int) {
       val currentItem = viewYourCattleModel[position]

        holder.yourCattleName.text = currentItem.animalType
        holder.yourCattleDescription.text = currentItem.cattleDescription

        //        get image from firebase store using glide
        Glide.with(context).load(currentItem.imageUrl).into(holder.yourCattleImage)



        holder.itemView.setOnClickListener {
            onYourCattleItemClick.yourCattleItemClick(currentItem)
        }
    }



    class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView){

        val yourCattleImage : ImageView = itemView.findViewById(R.id.card_cattle_image)
        val yourCattleName : TextView = itemView.findViewById(R.id.order_cattle_name)
        val yourCattleDescription : TextView = itemView.findViewById(R.id.order_cattle_description)

    }}

    interface OnYourCattleItemClick {
        fun  yourCattleItemClick(currentItem: ViewYourCattleModel)
    }
