package com.example.cattle_mall

import android.graphics.Bitmap

data class HomeCattleModel(

    var animalType: String? = null,
    var animalLocatation: String? = null,
    var currentMilk: String? = null,
    var milkCapacity: String? = null,
    var cattlePrice: String? = null,
    var deliveryStatus: String? = null,
    var pregnancyStatus: String? = null,
    var animalGender : String? = null,
    var cattleDescription : String? = null,
    var sellerId : String? = null,
    var imageUrl : String? = null,
    var sellername : String? = null,
    var sellerstate: String? = null,
    var sellerdistrict : String? = null,
    var sellid : String? = null,
    var type : String? = null,

    )
