package com.example.cattle_mall

import android.annotation.SuppressLint
import android.content.Intent
import android.content.SharedPreferences
import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.firestore.FirebaseFirestore
import com.google.firebase.firestore.ktx.firestore
import com.google.firebase.firestore.ktx.toObject
import com.google.firebase.ktx.Firebase

class ProfileFragment : Fragment(),OnOrderItemClick {
    private lateinit var sharedPreferences: SharedPreferences
    lateinit var auth: FirebaseAuth

    private lateinit var firebaseFirestore: FirebaseFirestore
    private lateinit var adapter: CattleOrderAdapter
    private lateinit var orderRecyclerView: RecyclerView
    private lateinit var orderArrayList : List<OrderCattleModel>

    @SuppressLint("MissingInflatedId", "SetTextI18n")
    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?,
    ): View? {
        auth = FirebaseAuth.getInstance()
        //firebase firestore initialization
        firebaseFirestore = FirebaseFirestore.getInstance()

        val rootView = inflater.inflate(R.layout.fragment_profile, container, false)


        val logPref = activity?.getSharedPreferences("login", AppCompatActivity.MODE_PRIVATE)

        val lucation = rootView.findViewById<TextView>(R.id.location)
        val email = rootView.findViewById<TextView>(R.id.email)
        val number = rootView.findViewById<TextView>(R.id.number)
        val whatsApp = rootView.findViewById<TextView>(R.id.whatsApp)
        val logOut = rootView.findViewById<ImageView>(R.id.logout)
        val name = rootView.findViewById<TextView>(R.id.profile_name)
        val updatebty = rootView.findViewById<ImageView>(R.id.update_image)

        updatebty.setOnClickListener {
            activity.let {
                val intent=Intent(it,Eidt_Profile::class.java)
                startActivity(intent)
            }
        }
        val id = logPref?.getString("userId", "")


        val firebaseFirestore = Firebase.firestore
//        sharedPreferences.getString("userId","")
        firebaseFirestore.collection("users").document(id.toString()).get()
            .addOnSuccessListener {
                val userData = it.toObject<UsersDataModel>()
                email.text = userData?.email;
                number.text = userData?.number;
                whatsApp.text = userData?.number;
                lucation.text =
                      userData?.state + ", " + userData?.district + ", " + userData?.village + ", " + userData?.pin;
                name.text = userData?.username;
            }.addOnFailureListener {
                Toast.makeText(activity, "failed", Toast.LENGTH_SHORT).show()
            }

        logOut.setOnClickListener {
            val logPref = activity?.getSharedPreferences("login", AppCompatActivity.MODE_PRIVATE)
            logPref?.edit()
            logPref?.edit()?.putBoolean("lStatuskey", false)?.commit()
            auth.signOut()
            activity.let {
                val intent = Intent(it, login_page::class.java)
                startActivity(intent)
            }

        }

        return rootView
    }
    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        val layoutManager = LinearLayoutManager(context)
        orderRecyclerView = view.findViewById(R.id.profil_recyclerView)
        orderRecyclerView.layoutManager = layoutManager

        dataInitialize()

    }

    @SuppressLint("ResourceType", "NotifyDataSetChanged")
    fun dataInitialize(){
        orderArrayList = arrayListOf<OrderCattleModel>()
        firebaseFirestore.collection("order").whereEqualTo("client_id", FirebaseAuth.getInstance().currentUser?.uid).get()
            .addOnSuccessListener {
                val checkResult = it.toObjects(OrderCattleModel::class.java)
                orderArrayList = checkResult
                adapter = activity?.let { it1 -> CattleOrderAdapter(this,orderArrayList, this) }!!
                orderRecyclerView.adapter = adapter
            }
            .addOnFailureListener {
                Toast.makeText(activity, it.message, Toast.LENGTH_SHORT).show()
            }

//        firebaseFirestore.collection("users").get()
//            .addOnSuccessListener {
//                val checkResult = it.toObjects(OrderCattleModel::class.java)
//                orderArrayList = checkResult
//                adapter = CattleOrderAdapter(this,orderArrayList, this)
//                orderRecyclerView.adapter?.notifyDataSetChanged()
//            }
//            .addOnFailureListener {
//                Toast.makeText(activity, it.message, Toast.LENGTH_SHORT).show()
//            }

    }

    override fun onOrderItemClick(currentItem: OrderCattleModel) {
        activity.let {
            val intent=Intent(it,ordercattledetails::class.java)
            intent.putExtra("order_id",currentItem.order_id)
            intent.putExtra("sell_id",currentItem.sell_id)
            intent.putExtra("seller_id",currentItem.seller_id)
            startActivity(intent)
        }
    }



}

