package com.example.cattle_mall

import android.annotation.SuppressLint
import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.ImageView
import android.widget.Toast
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.firestore.FirebaseFirestore
import kotlin.math.log

class ViewYourCattleSellsKt : AppCompatActivity(), OnYourCattleItemClick {

    lateinit var id : String
    private lateinit var firebaseFirestore: FirebaseFirestore
    private lateinit var yourCattleAdapter: ViewYourCattleAdapter
    private lateinit var yourCattleRecyclerView: RecyclerView
    private lateinit var yourCattleArrayList : ArrayList<ViewYourCattleModel>
    @SuppressLint("MissingInflatedId")
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.view_your_cattle_sells)

        val logPref = getSharedPreferences("login", AppCompatActivity.MODE_PRIVATE)

  id= logPref.getString("userId","").toString()

        val backHome = findViewById<ImageView>(R.id.back_homepage)
        backHome.setOnClickListener {
            onBackPressed()
        }

        yourCattleArrayList = ArrayList<ViewYourCattleModel>()

        firebaseFirestore = FirebaseFirestore.getInstance()
        val layoutManager = LinearLayoutManager(this)
        yourCattleRecyclerView = findViewById(R.id.your_cattle_recyclerview)
        yourCattleRecyclerView.layoutManager = layoutManager

        dataInitialize()

    }


    @SuppressLint("ResourceType", "NotifyDataSetChanged")
    fun dataInitialize(){
        firebaseFirestore.collection("sell").whereEqualTo("sellerid",id).get()
            .addOnSuccessListener {
                val checkResult = it.toObjects(ViewYourCattleModel::class.java)

                yourCattleArrayList.addAll(checkResult)

                yourCattleAdapter = ViewYourCattleAdapter(this, yourCattleArrayList, this)
                yourCattleRecyclerView.adapter = yourCattleAdapter
            }
            .addOnFailureListener {
                Toast.makeText(this, it.message, Toast.LENGTH_SHORT).show()
            }

//        firebaseFirestore.collection("users").get()
//            .addOnSuccessListener {
//                val checkResult = it.toObjects(ViewYourCattleModel::class.java)
//                yourCattleArrayList.addAll(checkResult)
//                yourCattleAdapter = ViewYourCattleAdapter(this, yourCattleArrayList, this)
//                yourCattleRecyclerView.adapter?.notifyDataSetChanged()
//            }
//            .addOnFailureListener {
//                Toast.makeText(this, it.message, Toast.LENGTH_SHORT).show()
//            }

    }

    override fun yourCattleItemClick(currentItem: ViewYourCattleModel) {
        val intent = Intent(this, sellcattlesdetails::class.java)
        intent.putExtra("id", currentItem.sellid)
        startActivity(intent)    }


}

