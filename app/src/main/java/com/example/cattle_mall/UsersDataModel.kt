package com.example.cattle_mall

data class UsersDataModel(
    val username: String = "",
    val email: String = "",
    var password: String = "",
    var number: String = "",
    var village: String = "",
    var pin: String = "",
    var district: String = "",
    var state: String = "",
)
