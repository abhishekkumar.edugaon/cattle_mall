package com.example.cattle_mall

import android.annotation.SuppressLint
import android.content.Context
import android.content.Intent
import android.content.SharedPreferences
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Button
import android.widget.TextView
import android.widget.Toast
import androidx.cardview.widget.CardView
import com.google.android.gms.auth.api.signin.GoogleSignIn
import com.google.android.gms.auth.api.signin.GoogleSignInOptions
import com.google.android.gms.common.SignInButton
import com.google.android.material.textfield.TextInputEditText
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.auth.GoogleAuthProvider

class login_page : AppCompatActivity() {

    lateinit  var auth : FirebaseAuth
    private  var requestCode = 1234


  lateinit var   logPref: SharedPreferences
    @SuppressLint("MissingInflatedId", "SuspiciousIndentation")
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.login_page)
        auth = FirebaseAuth.getInstance()
        logPref = getSharedPreferences("login", Context.MODE_PRIVATE)



        val  registerTest=findViewById<TextView>(R.id.register_now)
        registerTest.setOnClickListener {
            val intent=Intent(this,register_page_kt::class.java)
            startActivity(intent)
        }
        val googleSignInOptions = GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN)
            .requestIdToken(getString(R.string.default_web_client_id))
            .requestEmail()
            .build()

        val googleSignInClient = GoogleSignIn.getClient(this, googleSignInOptions)

        val email=findViewById<TextInputEditText>(R.id.Emailforlogin)
        val password=findViewById<TextInputEditText>(R.id.passwordforlogin)
        val goolge_bt=findViewById<CardView>(R.id.google_sign_in)
        goolge_bt.setOnClickListener {
            val googleIntent = googleSignInClient.signInIntent
            startActivityForResult(googleIntent, requestCode)

        }

        val button=findViewById<Button>(R.id.login_btn)
        button.setOnClickListener {
            if(email.text?.isEmpty() == false &&
                password.text?.isEmpty() == false)
            {

                auth.signInWithEmailAndPassword(
                    email.text.toString(), password.text.toString()

                )
                    .addOnSuccessListener {
                        Toast.makeText(this, "login successful", Toast.LENGTH_SHORT).show()
                        logPref.edit().putBoolean("lStatuskey", true).commit()
                        logPref.edit().putString("userId", it.user?.uid).commit()


                        // after login user go to bottomnavigationbar
                        val intent = Intent(this, BottomNavigationBarKt::class.java)
                        startActivity(intent)
                        finish()

                    }
                    .addOnFailureListener {
                        Toast.makeText(this, "Invelid User", Toast.LENGTH_SHORT).show()

                    }
            }
            else{
                Toast.makeText(this, "please fill email password", Toast.LENGTH_SHORT).show()

            }

        }

    }

    override fun onActivityResult(activityRequestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(activityRequestCode, resultCode, data)

        if (activityRequestCode == requestCode) {
            val task = GoogleSignIn.getSignedInAccountFromIntent(data)
            task.addOnSuccessListener { it ->
                val credencial = GoogleAuthProvider.getCredential(it.idToken, null)
                auth.signInWithCredential(credencial)
                    .addOnSuccessListener {
                        startActivity(Intent(this, BottomNavigationBarKt::class.java))
                        Toast.makeText(this, "successfuly" + it.user?.displayName, Toast.LENGTH_SHORT).show()
                        logPref.edit().putBoolean("lStatuskey", true).commit()
                        logPref.edit().putString("userId", it.user?.uid).commit()
                        val intent=Intent(this,BottomNavigationBarKt::class.java)
                        startActivity(intent)
                    }
                    .addOnFailureListener {
                        Toast.makeText(this, "hii" + it.message, Toast.LENGTH_SHORT).show()
                    }
            }
                .addOnFailureListener {
                    Toast.makeText(this, "hello" + it.message, Toast.LENGTH_SHORT).show()


                }
        }
    }






}